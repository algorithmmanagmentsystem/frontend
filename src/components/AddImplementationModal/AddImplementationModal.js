import { useCallback, useState, useEffect, useRef } from 'react';
import Modal, { ModalBody, ModalHeader, ModalFooter } from '../Modal';
import Button from '../Button';
import Input from '../Input';

const AddImplementationModal = ({ onSave, onClose, show, loading }) => {
  const [implementationName, setImplementationName] = useState('');
  const [code, setCode] = useState('');
  const [codeFile, setCodeFile] = useState('');
  const [codeFileFormat, setCodeFileFormat] = useState('');
  const [codeFileExtension, setCodeFileExtension] = useState('');
  const [codeMimeType, setCodeMimeType] = useState('');

  const [isUpload, setIsUpload] = useState(true);

  const fileRef = useRef(null);

  const handleCancel = useCallback(() => {
    setImplementationName('');
    setCode('');
    onClose();
  }, [onClose, setImplementationName]);

  const handleSave = () => {
    onSave(
      implementationName,
      isUpload ? codeFile : code,
      isUpload,
      isUpload ? codeFileFormat : codeFileExtension,
      codeMimeType,
    );
  };

  const handleChange = (e) => {
    setImplementationName(e.target.value);
  };

  const handleCodeChange = (e) => {
    setCode(e.target.value);
  };

  const handleFileChange = (e) => {
    const { files } = e.target;
    if (files.length === 0) {
      return;
    }
    const file = files[0];

    let reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = () => {
      setCodeFile(reader.result);
      setCodeMimeType(file.type);
      const fname = file.name.split('.');
      setCodeFileFormat(fname[fname.length - 1]);
    };
  };

  const handleImplementationTypeSwitch = () => {
    setIsUpload(!isUpload);
  };

  useEffect(() => {
    if (!show) {
      setImplementationName('');
      setCode('');
      setCodeFileFormat('');
      setCodeFileExtension('');
      setCodeMimeType('');
      if (fileRef && fileRef.current) {
        fileRef.current.value = null;
      }
    }
  }, [fileRef, show, setImplementationName, setCode]);

  const handleCodeFileExtensionChange = (e) => {
    setCodeFileExtension(e.target.value);
  };

  return (
    <Modal show={show}>
      <ModalHeader>Add Implementation</ModalHeader>
      <ModalBody>
        <Input
          label="Implementation Name"
          name="implementationName"
          onChange={handleChange}
          value={implementationName}
        />
        <div className="form-check form-switch">
          <input
            className="form-check-input"
            type="checkbox"
            id="implementationSwitch"
            onChange={handleImplementationTypeSwitch}
            checked={isUpload}
          />
          <label className="form-check-label" htmlFor="implementationSwitch">
            Upload Implementation Code as File
          </label>
        </div>
        {isUpload ? (
          <Input ref={fileRef} type="file" label="Upload File" name="codeFile" onChange={handleFileChange} />
        ) : (
          <>
            <Input type="textarea" label="Implementation Code" name="code" onChange={handleCodeChange} value={code} />
            <Input
              label="File Extension"
              placeholder="eg: py, c, cpp, java"
              name="codeFileExtension"
              onChange={handleCodeFileExtensionChange}
              value={codeFileExtension}
            />
          </>
        )}
      </ModalBody>
      <ModalFooter>
        <Button type="primary" onClick={handleSave} loading={loading}>
          {loading ? 'Saving' : 'Save'}
        </Button>
        <Button onClick={handleCancel}>Cancel</Button>
      </ModalFooter>
    </Modal>
  );
};

export default AddImplementationModal;
