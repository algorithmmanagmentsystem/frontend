import { useState, forwardRef } from 'react';
import './select.scss';

const Select = ({ label = '', onChange, options = [], value, placeholder = '' }, ref) => {
  const [labelId] = useState(() => {
    return `select-${Math.floor(Math.random() * 1000000)}`;
  });

  console.log(value)
  return (
    <div className="select mb-2">
      {label && <label htmlFor={labelId}>{label}</label>}
      <select ref={ref} id={labelId} className="form-select" onChange={onChange} value={value || ''}>
        {options.map((option) => (
          <option key={option.value} value={option.value}>{option.label}</option>
        ))}
      </select>
    </div>
  );
};

export default forwardRef(Select);
