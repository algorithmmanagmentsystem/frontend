import { useCallback, useState, useEffect } from 'react';
import Modal, { ModalBody, ModalHeader, ModalFooter } from '../Modal';
import Button from '../Button';
import Input from '../Input';

const AddClassificationModal = ({ onSave, onClose, show, loading }) => {
  const [classificationName, setClassificatonName] = useState('');

  const handleCancel = useCallback(() => {
    setClassificatonName('');
    onClose();
  }, [onClose, setClassificatonName]);

  const handleSave = () => {
    onSave(classificationName);
  };

  const handleChange = (e) => {
    setClassificatonName(e.target.value);
  };

  useEffect(() => {
    if (!show) {
      setClassificatonName('')
    }
  }, [show, setClassificatonName])

  return (
    <Modal show={show}>
      <ModalHeader>Add Classification</ModalHeader>
      <ModalBody>
        <Input
          label="Classification Name"
          name="classificationName"
          onChange={handleChange}
          value={classificationName}
        />
      </ModalBody>
      <ModalFooter>
        <Button type="primary" onClick={handleSave} loading={loading}>
          {loading ? 'Saving' : 'Save'}
        </Button>
        <Button onClick={handleCancel}>Cancel</Button>
      </ModalFooter>
    </Modal>
  );
};

export default AddClassificationModal;
