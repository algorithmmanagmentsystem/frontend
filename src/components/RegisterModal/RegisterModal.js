import { useCallback, useState, useEffect } from 'react';
import Modal, { ModalBody, ModalHeader, ModalFooter } from '../Modal';
import Button from '../Button';
import Input from '../Input';

const RegisterModal = ({ onRegister, onClose, show, loading }) => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [verifyPassword, setVerifyPassword] = useState('');

  const handleCancel = useCallback(() => {
    setName('');
    setEmail('');
    setUsername('');
    setPassword('');
    setVerifyPassword('');
    onClose();
  }, [onClose]);

  const handleLogin = () => {
    onRegister(name, email, username, password);
  };

  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const handleVerifyPasswordChange = (e) => {
    setVerifyPassword(e.target.value);
  };

  useEffect(() => {
    if (!show) {
      setUsername('');
      setPassword('');
    }
  }, [show, setUsername, setPassword]);

  return (
    <Modal show={show}>
      <ModalHeader>Register</ModalHeader>
      <ModalBody>
        <Input label="Name" name="name" onChange={handleNameChange} value={name} />
        <Input label="Email" name="email" onChange={handleEmailChange} value={email} />
        <Input label="Username" name="username" onChange={handleUsernameChange} value={username} />
        <Input type="password" label="Password" name="password" onChange={handlePasswordChange} value={password} />
        {/* <Input type="password" label="Verify Password" name="verifyPassword" onChange={handleVerifyPasswordChange} value={verifyPassword} /> */}
      </ModalBody>
      <ModalFooter>
        <Button type="primary" onClick={handleLogin} loading={loading}>
          {loading ? 'Signing Up' : 'Sign Up'}
        </Button>
        <Button onClick={handleCancel}>Cancel</Button>
      </ModalFooter>
    </Modal>
  );
};

export default RegisterModal;
