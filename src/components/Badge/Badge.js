function Badge({ type }) {
  let badgeClassName = 'badge rounded-pill';
  let text = ''
  switch (type) {
    case 'classification':
      badgeClassName += ' bg-primary'
      text = 'Classification'
      break
    case 'algorithm':
      badgeClassName += ' bg-danger'
      text = 'Algorithm'
      break
    case 'implementation':
      badgeClassName += ' bg-warning'
      text = 'Implementation'
      break
    default:      
      badgeClassName += ' bg-secondary'
  }
  return (
    <div className={badgeClassName}>
      {text}
    </div>
  );
}

export default Badge;
