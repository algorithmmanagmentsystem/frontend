import './button.scss';

const Button = ({ children, type, onClick, disabled, size, className, noBorder, loading }) => {
  let btnClass = 'btn align-items-center';
  switch (type) {
    case 'outline-primary':
      btnClass += ' btn-outline-primary';
      break;
    case 'outline-secondary':
      btnClass += ' btn-outline-secondary';
      break;
    case 'outline-danger':
      btnClass += ' btn-outline-danger';
      break;
    case 'dotted':
      btnClass += ' btn-dotted';
      break;
    case 'primary':
      btnClass += ' btn-primary';
      break;
    case 'secondary':
      btnClass += ' btn-secondary';
      break;
    case 'link':
      btnClass += ' btn-link';
      break;
    default:
      btnClass += ' btn-outline-dark';
  }
  if (size === 'small') {
    btnClass += ' btn-sm';
  }
  if (noBorder) {
    btnClass += ` btn-no-border`;
  }
  if (className) {
    btnClass += ` ${className}`;
  }
  return (
    <button className={btnClass} onClick={onClick} disabled={disabled || loading}>
      {loading && (
        <div className="spinner-border spinner-border-sm text-light me-2" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      )}
      {children}
    </button>
  );
};

export default Button;
