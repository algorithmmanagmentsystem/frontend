import { useCallback, useState, useEffect } from 'react';
import Modal, { ModalBody, ModalHeader, ModalFooter } from '../Modal';
import Button from '../Button';
import Input from '../Input';

const AddAlgorithmModal = ({ onSave, onClose, show, loading }) => {
  const [algorithmName, setAlgorithmName] = useState('');

  const handleCancel = useCallback(() => {
    setAlgorithmName('');
    onClose();
  }, [onClose, setAlgorithmName]);

  const handleSave = () => {
    onSave(algorithmName);
  };

  const handleChange = (e) => {
    setAlgorithmName(e.target.value);
  };

  useEffect(() => {
    if (!show) {
      setAlgorithmName('')
    }
  }, [show, setAlgorithmName])

  return (
    <Modal show={show}>
      <ModalHeader>Add Algorithm</ModalHeader>
      <ModalBody>
        <Input
          label="Algorithm Name"
          name="algorithmName"
          onChange={handleChange}
          value={algorithmName}
        />
      </ModalBody>
      <ModalFooter>
        <Button type="primary" onClick={handleSave} loading={loading}>
          {loading ? 'Saving' : 'Save'}
        </Button>
        <Button onClick={handleCancel}>Cancel</Button>
      </ModalFooter>
    </Modal>
  );
};

export default AddAlgorithmModal;
