import { useCallback, useState, useEffect } from 'react';
import Modal, { ModalBody, ModalHeader, ModalFooter } from '../Modal';
import Button from '../Button';
import Input from '../Input';
import Badge from '../Badge';
import './algorithmModal.scss';

const AlgorithmModal = ({
  algorithmName,
  problemInstances,
  onDeleteProblemInstance,
  onSaveProblemInstance,
  onClose,
  show,
  loading,
  user,
}) => {
  const [name, setName] = useState('');
  const [inputContent, setInputContent] = useState('');

  const handleCancel = useCallback(() => {
    setName('');
    setInputContent('');
    onClose();
  }, [onClose, setInputContent, setName]);

  const handleSave = () => {
    onSaveProblemInstance(name, inputContent);
  };

  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const handleInputContentChange = (e) => {
    setInputContent(e.target.value);
  };

  useEffect(() => {
    if (!show) {
      setInputContent('');
      setName('');
    }
  }, [show, setInputContent]);

  useEffect(() => {
    if (!loading) {
      setInputContent('');
      setName('');
    }
  }, [loading, setInputContent, setName]);

  return (
    <Modal show={show}>
      <ModalHeader>
        {algorithmName}
        <Badge type="algorithm" />
      </ModalHeader>
      <ModalBody>
        {problemInstances.length ? (
          <div className="border-bottom mb-2 pb-2">
            <h5>Problem Instances</h5>
            {problemInstances.map((problemInstance) => (
              <div key={problemInstance.id} className="problem-instance">
                <div className="problem-instance__name">{problemInstance.name}</div>
                <div className="problem-instance__input">{problemInstance.input}</div>
                <Button
                  type="outline-danger"
                  size="small"
                  onClick={() => {
                    onDeleteProblemInstance(problemInstance.id);
                  }}
                >
                  Delete
                </Button>
              </div>
            ))}
          </div>
        ) : (
          <div>There are no problem instances added for the algorithm. {user && 'Try adding some...'}</div>
        )}
        {user && (
          <div>
            <h2 className="h5">Add Problem Instance</h2>
            <Input label="Problem Instance Name" name="name" onChange={handleNameChange} value={name} />
            <Input
              type="textarea"
              label="Input"
              name="inputContent"
              onChange={handleInputContentChange}
              value={inputContent}
            />
            <Button type="primary" onClick={handleSave} loading={loading}>
              {loading ? 'Saving' : 'Save'}
            </Button>
          </div>
        )}
      </ModalBody>
      <ModalFooter>
        {/* {user ? (
          <>
            <Button type="primary" onClick={handleSave} loading={loading}>
              {loading ? 'Saving' : 'Save'}
            </Button>
            <Button onClick={handleCancel}>Cancel</Button>
          </>
        ) : (
          )} */}
        <Button onClick={handleCancel}>Close</Button>
      </ModalFooter>
    </Modal>
  );
};

export default AlgorithmModal;
