import { useEffect } from 'react';

import './modal.scss';

const Modal = ({ children, show, size = 'md' }) => {
  const dialogClass = `modal-dialog modal-dialog-centered modal-dialog-scrollable modal-${size}`;
  useEffect(() => {
    if (show) {
      document.body.classList.add('modal-open');
    } else {
      document.body.classList.remove('modal-open');
    }
  }, [show]);

  return (
    <div className={`modal ${show ? 'modal--open' : ''}`}>
      <div className="modal-background"></div>
      <div className={dialogClass}>
        <div className="modal-content">{children}</div>
      </div>
    </div>
  );
};

const ModalHeader = ({ children, showCloseButton }) => {
  return <div className="modal-header">{children}</div>;
};

const ModalBody = ({ children }) => {
  return <div className="modal-body">{children}</div>;
};

const ModalFooter = ({ children }) => {
  return <div className="modal-footer">{children}</div>;
};

export default Modal;
export { ModalHeader, ModalBody, ModalFooter };
