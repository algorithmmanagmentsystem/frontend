import Modal, { ModalHeader, ModalBody, ModalFooter } from './Modal';
export default Modal;
export { ModalHeader, ModalBody, ModalFooter };
