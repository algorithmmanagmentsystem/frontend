import { useCallback, useState, useEffect } from 'react';
import Modal, { ModalBody, ModalHeader, ModalFooter } from '../Modal';
import Button from '../Button';
import Input from '../Input';
import Badge from '../Badge';
import './implementationModal.scss';
import Select from '../Select';

const ImplementationModal = ({
  implementation,
  problemInstances,
  benchmarks,
  onDeleteBenchmark,
  onSaveBenchmark,
  onClose,
  show,
  loading,
  user,
}) => {
  const [problemInstanceOptions, setProblemInstanceOptions] = useState([]);

  const [problemInstanceId, setProblemInstanceId] = useState('');

  const [processor, setProcessor] = useState('');
  const [ram, setRam] = useState('');
  const [cores, setCores] = useState('');
  const [l1Cache, setL1Cache] = useState('');
  const [l2Cache, setL2Cache] = useState('');
  const [l3Cache, setL3Cache] = useState('');
  const [result, setResult] = useState('');

  const clearForm = useCallback(() => {
    setProblemInstanceId(null);
    setProcessor('');
    setRam('');
    setCores('');
    setL1Cache('');
    setL2Cache('');
    setL3Cache('');
    setResult('');
  }, []);

  useEffect(() => {
    setProblemInstanceOptions(
      problemInstances.map((instance) => ({
        label: instance.name,
        value: instance.id,
      })),
    );
  }, [problemInstances]);

  const handleCancel = useCallback(() => {
    clearForm();
    onClose();
  }, [onClose, clearForm]);

  const handleSave = () => {
    console.log(implementation.id, problemInstanceId, processor, ram, cores, l1Cache, l2Cache, l3Cache, result);
    // onSaveBenchmark(implementation.id, problemInstanceId, processor, ram, cores, l1Cache, l2Cache, l3Cache, result);
  };

  useEffect(() => {
    if (!show) {
      clearForm();
    }
  }, [show, clearForm]);
  console.log(implementation);
  console.log(problemInstances);
  console.log(benchmarks);

  const handleProblemInstanceChange = useCallback((e) => {
    setProblemInstanceId(e.target.value);
  }, []);

  const handleProcessorChange = useCallback((e) => {
    setProcessor(e.target.value);
  }, []);

  const handleRamChange = useCallback((e) => {
    setRam(e.target.value);
  }, []);

  const handleCoresChange = useCallback((e) => {
    setCores(e.target.value);
  }, []);

  const handleL1CacheChange = useCallback((e) => {
    setL1Cache(e.target.value);
  }, []);

  const handleL2CacheChange = useCallback((e) => {
    setL2Cache(e.target.value);
  }, []);

  const handleL3CacheChange = useCallback((e) => {
    setL3Cache(e.target.value);
  }, []);

  const handleResultChange = useCallback((e) => {
    setResult(e.target.value);
  }, []);

  return (
    <Modal show={show} size="xl">
      <ModalHeader>
        {implementation.name}
        <a href={implementation.s3Url} download>
          {/* Download */}
          <Button>Download Implementation</Button>
        </a>

        <Badge type="implementation" />
      </ModalHeader>
      <ModalBody>
        <div className="row">
          <div className="col">
            <Select
              label="Problem Instance"
              options={problemInstanceOptions}
              value={problemInstanceId}
              onChange={handleProblemInstanceChange}
            />
            <Input label="Procesor" name="processor" onChange={handleProcessorChange} value={processor} />
            <Input label="Ram" name="ram" onChange={handleRamChange} value={ram} />
            <Input label="Cores" name="cores" onChange={handleCoresChange} value={cores} />
            <Input label="L1 Cache" name="l1Cache" onChange={handleL1CacheChange} value={l1Cache} />
            <Input label="L2 Cache" name="l1Cache" onChange={handleL2CacheChange} value={l2Cache} />
            <Input label="L3 Cache" name="l1Cache" onChange={handleL3CacheChange} value={l3Cache} />
            <Input
              label="Result (in ms)"
              name="l1Cache"
              onChange={handleResultChange}
              value={result}
              placeholder="eg: 1250"
            />
            <div>
              <Button type="primary" onClick={handleSave} loading={loading}>
                {loading ? 'Saving' : 'Save'}
              </Button>
            </div>
          </div>
          <div className="col">
            {benchmarks.map((benchmark) => (
              <div>Benchmark</div>
            ))}
          </div>
        </div>
      </ModalBody>
      <ModalFooter>
        <Button onClick={handleCancel}>Close</Button>
      </ModalFooter>
    </Modal>
  );
};

export default ImplementationModal;
