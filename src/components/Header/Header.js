import { useEffect, useState } from 'react';
import Button from '../Button';
import LoginModal from '../LoginModal';
import RegisterModal from '../RegisterModal';
import userIcon from '../../images/user.png';

import './header.scss';
import { Link } from 'react-router-dom';

function Header({ user, onLogin, loginLoading, registerLoading, onRegister, onSignOut }) {
  const [loginModalOpen, setLoginModalOpen] = useState(false);
  const [registerModalOpen, setRegisterModalOpen] = useState(false);

  const handleOpenLogin = () => {
    setLoginModalOpen(true);
  };

  const handleCloseLogin = () => {
    setLoginModalOpen(false);
  };

  const handleOpenRegister = () => {
    setRegisterModalOpen(true);
  };

  const handleCloseRegister = () => {
    setRegisterModalOpen(false);
  };

  useEffect(() => {
    if (user) {
      setLoginModalOpen(false);
      setRegisterModalOpen(false);
    }
  }, [user]);

  return (
    <div className="header">
      <div className="container">
        <div className="row">
          <div className="col d-flex align-items-center">
            <div className="header__brand">AlgoClass</div>
            {user && user.isAdmin && <div className="ms-4"><Link to="/" className="d-inline-block"><Button type="link">Home</Button></Link><Link to="/users" className="d-inline-block"><Button type="link">Users</Button></Link></div>}
          </div>
          <div className="col d-flex justify-content-end align-items-center">
            {user && (
              <div className="d-flex align-items-center me-2">
                <img src={userIcon} alt="user" height="20" className="me-2" /> {user.username}
              </div>
            )}
            {user ? (
              <Button type="secondary-outline" onClick={onSignOut}>
                Log Out
              </Button>
            ) : (
              <>
                <Button type="primary" onClick={handleOpenLogin}>
                  Log In
                </Button>
                <Button type="secondary-outline" onClick={handleOpenRegister} className="ms-2">
                  Register
                </Button>
              </>
            )}
          </div>
        </div>
      </div>
      <LoginModal show={loginModalOpen} loading={loginLoading} onLogin={onLogin} onClose={handleCloseLogin} />
      <RegisterModal
        show={registerModalOpen}
        loading={registerLoading}
        onRegister={onRegister}
        onClose={handleCloseRegister}
      />
    </div>
  );
}

export default Header;
