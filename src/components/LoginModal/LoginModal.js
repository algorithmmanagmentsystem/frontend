import { useCallback, useState, useEffect } from 'react';
import Modal, { ModalBody, ModalHeader, ModalFooter } from '../Modal';
import Button from '../Button';
import Input from '../Input';

const LoginModal = ({ onLogin, onClose, show, loading }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleCancel = useCallback(() => {
    setUsername('');
    setPassword('');
    onClose();
  }, [onClose, setUsername, setPassword]);

  const handleLogin = () => {
    onLogin(username, password);
  };

  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  useEffect(() => {
    if (!show) {
      setUsername('');
      setPassword('');
    }
  }, [show, setUsername, setPassword]);

  useEffect(() => {
    if (!loading) {
      setPassword('');
    }
  }, [loading, setPassword]);

  return (
    <Modal show={show}>
      <ModalHeader>User Login</ModalHeader>
      <ModalBody>
        <Input label="Username" name="username" onChange={handleUsernameChange} value={username} />
        <Input type="password" label="Password" name="password" onChange={handlePasswordChange} value={password} />
      </ModalBody>
      <ModalFooter>
        <Button type="primary" onClick={handleLogin} loading={loading}>
          {loading ? 'Logging In' : 'Log In'}
        </Button>
        <Button onClick={handleCancel}>Cancel</Button>
      </ModalFooter>
    </Modal>
  );
};

export default LoginModal;
