import { useState, forwardRef } from 'react';
import './input.scss';

const Input = ({ type = 'text', label = '', onChange, value, placeholder = '' }, ref) => {
  const [labelId] = useState(() => {
    return `input-${Math.floor(Math.random() * 1000000)}`;
  });
  return (
    <div className="input mb-2">
      {label && <label htmlFor={labelId}>{label}</label>}
      {type === 'textarea' ? (
        <textarea ref={ref} id={labelId} className="form-control" onChange={onChange} value={value} />
      ) : (
        <input
          ref={ref}
          id={labelId}
          type={type}
          className="form-control"
          onChange={onChange}
          value={value}
          placeholder={placeholder}
        />
      )}
    </div>
  );
};

export default forwardRef(Input);
