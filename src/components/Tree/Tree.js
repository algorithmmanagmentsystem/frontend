import './tree.scss';

function Tree({ children, level = 0, className }) {
  return <div className={`tree tree--level${level} ${className || ''}`}>{children}</div>;
}

export const TreeHeader = ({ children, dashed = false, onClick }) => {
  const handleClick = (e) => {
    if (onClick) {
      onClick(e);
    }
  };
  return (
    <div className={`tree__header ${dashed ? 'tree__header--dashed' : ''}`} onClick={handleClick}>
      {children}
    </div>
  );
};

export const TreeBody = ({ children, isOpen, isLoading }) => {
  if (isOpen && isLoading) {
    return (
      <div className="tree__body tree__body--open tree__body--loading">
        <div className="spinner-border text-secondary" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      </div>
    );
  }
  return <div className={`tree__body ${isOpen ? 'tree__body--open' : ''}`}>{children}</div>;
};

export default Tree;
