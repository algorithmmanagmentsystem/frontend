import { postSignOut } from './account';

export const getToken = (auth) => {
  if (!auth) {
    return null;
  }
  const user = JSON.parse(localStorage.getItem('user'));
  return { token: user.idToken, payload: user.idTokenPayload };
};

async function checkStatus(response) {
  if (response.status === 401) {
    await postSignOut();
    window.location.replace('/');
    const error = new Error();
    error.response = response;
    throw error;
  }
  return response;
}

export default async function request(url, options = { method: 'GET', auth: false }) {
  try {
    const { auth, ...opt } = options;

    const user = getToken(auth);
    if (user && user.payload.exp - Date.now() / 1000 < 120) {
      await postSignOut();
      window.location.replace('/');
      const error = new Error('Expired');
      throw error;
    }

    const headers = {
      'Content-Type': 'application/json',
    };
    if (user) {
      headers.authorization = user.token;
    }

    return fetch(url, {
      ...opt,
      ...('body' in opt ? { body: JSON.stringify({ ...opt.body }) } : {}),
      headers,
    })
      .then(checkStatus)
      .then((r) => r.json());
  } catch (err) {
    console.log(err);
    return 'err';
  }
}
