import request from "./request";
import { createAlgorithmsRoute, deleteAlgorithmRoute, getAlgorithmsRoute, reClassifyAlgorithmRoute } from "./routes";

export const getAlgorithms = async (classificationId) => {
  const { list, statusCode, error } = await request(getAlgorithmsRoute(classificationId), {
    method: 'GET',
    auth: false,
  });
  if (statusCode !== 200) {
    alert(error);
    return null;
  }
  return list.map(algorithm => ({
    name: algorithm.name,
    id: algorithm.id,
  }))
};

export const postAlgorithm = async (classificationId, algorithmName) => {
  const { response, httpCode } = await request(createAlgorithmsRoute(classificationId), {
    method: 'POST',
    body: {
      nameAlgorithm: algorithmName,
      parentId: classificationId,
    },
    auth: true,
  });
  if (httpCode !== 200) {
    alert(response);
    return null;
  }
  return {
    id: response,
    name: algorithmName,
    classificationId,
  };
};

export const deleteAlgorithm = async (algorithmId) => {
  const { response, httpCode } = await request(deleteAlgorithmRoute(algorithmId), {
    method: 'POST',
    auth: true,
  });
  if (httpCode !== 200) {
    alert(response);
    return null;
  }
  return {
    response
  };
};

export const reClassifyAlgorithm = async (algorithmId, classificationId) => {
  const { response, httpCode } = await request(reClassifyAlgorithmRoute(), {
    method: 'POST',
    auth: true,
    body: {
      algorithmId,
      classificationId,
    }
  });
  if (httpCode !== 200) {
    alert(response);
    return null;
  }
  return {
    response
  };
};
