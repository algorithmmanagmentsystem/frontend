import request from "./request";
import { createImplementationRoute, deleteImplementationRoute, getImplementationsRoute } from "./routes";

export const getImplementations = async (algorithmId) => {
  const { list, statusCode, error } = await request(getImplementationsRoute(algorithmId), {
    method: 'GET',
    auth: false,
  });
  if (statusCode !== 200) {
    alert(error);
    return null;
  }
  return list.map(implementation => ({
    id: implementation.idImplementation,
    name: implementation.implementationFile,
    algorithmId: implementation.algorithmId,
    s3Url: `https://cs509-thalassa-algorithm-management-system.s3.us-east-2.amazonaws.com/${implementation.s3Url}`,
  }))
};

export const postImplementation = async (algorithmId, implementationName, code, isUpload, implementationFileFormat, implementationMimeType) => {
  const { response, httpCode } = await request(createImplementationRoute(), {
    method: 'POST',
    body: {
      implementationName,
      algorithmId,
      value: code,
      isUpload,
      implementationFileFormat,
      implementationMimeType,
    },
    auth: true,
  });
  if (httpCode !== 200) {
    alert(response);
    return null;
  }
  return {
    id: response,
    name: implementationName,
    algorithmId,
    s3Url: `https://cs509-thalassa-algorithm-management-system.s3.us-east-2.amazonaws.com/implementations/${response}.${implementationFileFormat}`
  };
};

export const deleteImplementation = async (implementationId) => {
  const { response, httpCode } = await request(deleteImplementationRoute(implementationId), {
    method: 'POST',
    auth: true,
  });
  if (httpCode !== 200) {
    alert(response);
    return null;
  }
  return {
    response
  };
};