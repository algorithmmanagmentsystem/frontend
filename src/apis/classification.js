import request from './request';
import {
  createClassificationsRoute,
  deleteClassificationRoute,
  getClassificationsRoute,
  mergeClassificationsRoute,
} from './routes';

export const getClassifications = async (id) => {
  const response = await request(getClassificationsRoute(id), {
    method: 'GET',
    auth: false,
  });

  return response.list.map((r) => ({
    id: r.id,
    name: r.nameClassification,
    parentId: r.parentClassification,
  }));
};

export const postClassification = async (classificationName, parentClassificationId) => {
  const { response, httpCode } = await request(createClassificationsRoute(), {
    method: 'POST',
    body: {
      nameClassification: classificationName,
      parentClassification: parentClassificationId,
    },
    auth: true,
  });
  if (httpCode !== 200) {
    alert(response);
    return null;
  }
  return {
    id: response,
    name: classificationName,
    parentId: parentClassificationId,
  };
};

export const deleteClassification = async (classificatioId) => {
  const { response, httpCode } = await request(deleteClassificationRoute(classificatioId), {
    method: 'POST',
    auth: true,
  });
  if (httpCode !== 200) {
    alert(response);
    return null;
  }
  return {
    response,
  };
};

export const mergeClassifications = async (classificationId1, classificationId2, name) => {
  const { response, httpCode } = await request(mergeClassificationsRoute(), {
    method: 'POST',
    auth: true,
    body: {
      classificationId1,
      classificationId2,
      name,
    },
  });
  if (httpCode !== 200) {
    alert(response);
    return null;
  }
  return {
    response,
  };
};
