import request from './request';
import { deleteUserRoute, getUserHistoryRoute, getUsersRoute } from './routes';

const formatDate = (date) => {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0' + minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear() + '  ' + strTime;
};

export const getUserHistory = async (userId) => {
  const { list, statusCode, error } = await request(getUserHistoryRoute(userId), {
    method: 'GET',
    auth: true,
  });
  if (statusCode !== 200) {
    alert(error);
    return null;
  }
  return list.map((user) => {
    let dateTimeParts = user.ActivityTime.split(/[- :]/);
    dateTimeParts[1]--;
    const d = new Date(...dateTimeParts);
    return {
      username: user.Username,
      id: user.UserID,
      activity: user.Activity,
      timestamp: user.ActivityTime,
      // timestamp: d,
      // formattedDate: formatDate(d),
    };
  });
};

export const getUsers = async () => {
  const { list, statusCode, error } = await request(getUsersRoute(), {
    method: 'POST',
    auth: true,
    body: {
      limit: 1000,
    },
  });
  if (statusCode !== 200) {
    alert(error);
    return null;
  }
  return list.map((user) => {
    return {
      id: user.id,
      username: user.email,
      email: user.username,
      name: user.name,
    };
  });
};


export const deleteUser = async (userId) => {
  const { response, statusCode, error } = await request(deleteUserRoute(), {
    method: 'POST',
    auth: true,
    body: {
      id: userId,
    },
  });
  if (statusCode !== 200) {
    alert(error);
    return null;
  }
  return response;
};

