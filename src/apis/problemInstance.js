import request from "./request";
import { createProblemInstancesRoute, deleteProblemInstancesRoute, getProblemInstancesRoute } from "./routes";

export const getProblemInstances = async (algorithmId) => {
  const { list, statusCode, error } = await request(getProblemInstancesRoute(algorithmId), {
    method: 'GET',
    auth: false,
  });
  if (statusCode !== 200) {
    alert(error);
    return null;
  }
  return list.map(problemInstance => ({
    id: problemInstance.id,
    algorithmId: problemInstance.algorithmId,
    name: problemInstance.name,
    input: problemInstance.input,
  }))
};

export const postProblemInstance = async (algorithmId, name, input) => {
  const { id, response, httpCode } = await request(createProblemInstancesRoute(), {
    method: 'POST',
    body: {
      algorithmId,
      name,
      input
    },
    auth: true,
  });
  if (httpCode !== 200) {
    alert(response);
    return null;
  }
  return {
    id,
    algorithmId,
    input,
    name,
  };
};

export const deleteProblemInstance = async (problemInstanceId) => {
  const { response, httpCode } = await request(deleteProblemInstancesRoute(problemInstanceId), {
    method: 'DELETE',
    // body: JSON.stringify({
    //   problemInstanceId,
    // }),
    auth: true,
  });
  if (httpCode !== 200) {
    alert(response);
    return null;
  }
  return {
    response
  };
};
