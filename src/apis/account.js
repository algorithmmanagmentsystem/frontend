import { CognitoUserPool, CognitoUserAttribute, CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js';
import request from './request';
import { internalSignUpRoute } from './routes';

const USERPOOLID = process.env.REACT_APP_COGNITO_USERPOOLID;
const CLIENTID = process.env.REACT_APP_COGNITO_CLIENTID;

const poolData = {
  UserPoolId: USERPOOLID,
  ClientId: CLIENTID,
};

const userPool = new CognitoUserPool(poolData);
let currentUser = userPool.getCurrentUser();

function getCognitoUser(username) {
  const userData = {
    Username: username,
    Pool: userPool,
  };
  const cognitoUser = new CognitoUser(userData);

  return cognitoUser;
}

export async function signUpUserWithEmail(name, email, username, password) {
  return new Promise(function (resolve, reject) {
    const attributeList = [
      new CognitoUserAttribute({
        Name: 'name',
        Value: name,
      }),
      new CognitoUserAttribute({
        Name: 'email',
        Value: email,
      }),
    ];

    userPool.signUp(username, password, attributeList, [], function (err, res) {
      if (err) {
        reject(err);
      } else {
        // console.log(res, "RESPONSE<<<<")
        resolve(res);
      }
    });
  }).catch((err) => {
    throw err;
  });
}

async function signInWithEmail(username, password) {
  return new Promise(function (resolve, reject) {
    const authenticationData = {
      Username: username,
      Password: password,
    };

    const authenticationDetails = new AuthenticationDetails(authenticationData);

    currentUser = getCognitoUser(username);

    currentUser.authenticateUser(authenticationDetails, {
      onSuccess: function (res) {
        resolve(res);
      },
      onFailure: function (err) {
        reject(err);
      },
    });
  }).catch((err) => {
    throw err;
  });
}

export const postLogin = async (username, password) => {
  try {
    const loginResponse = await signInWithEmail(username, password);
    const data = {
      username,
      idToken: loginResponse.getIdToken().jwtToken,
      idTokenPayload: loginResponse.getIdToken().payload,
      accessToken: loginResponse.getAccessToken().jwtToken,
      refreshToken: loginResponse.getRefreshToken().token,
      isAdmin: (loginResponse.getIdToken().payload['cognito:groups'] || []).indexOf('Admin') !== -1,
    };
    localStorage.setItem('user', JSON.stringify(data));
    return data;
  } catch (err) {
    throw err;
  }
};

export const internalSignUp = async (userId, username, email, name) => {
  const { response, httpCode } = await request(internalSignUpRoute(), {
    method: 'POST',
    body: {
      id: userId,
      username,
      email,
      name,
    },
    auth: false,
  });
  if (httpCode !== 200) {
    alert(response);
    return null;
  }
  return {
    userId,
  };
};

const sleep = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

export const postRegister = async (name, email, username, password) => {
  try {
    const userSignUpData = await signUpUserWithEmail(name, email, username, password);

    await internalSignUp(userSignUpData.userSub, username, email, name);

    await sleep(2000);

    const loginResponse = await postLogin(username, password);

    return loginResponse;
  } catch (err) {
    throw err;
  }
};

export const postSignOut = async () => {
  try {
    localStorage.removeItem('user');
    const user = userPool.getCurrentUser();
    user.signOut();
  } catch (err) {
    console.log(err);
  }
};
