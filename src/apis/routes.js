const BASEROUTE = process.env.REACT_APP_BASEROUTE;

export const CLASSIFICATION_ROUTE = `${BASEROUTE}/classifications`;

export const getClassificationsRoute = (id) => {
  return id ? `${CLASSIFICATION_ROUTE}/${id}` : CLASSIFICATION_ROUTE;
};

export const deleteClassificationRoute = (classificationId) => {
  return `${BASEROUTE}/classifications/delete/${classificationId}`;
};

export const mergeClassificationsRoute = () => {
  return `${BASEROUTE}/classifications/merge`;
};

export const createClassificationsRoute = () => {
  return CLASSIFICATION_ROUTE;
};

export const getAlgorithmsRoute = (id) => {
  return `${CLASSIFICATION_ROUTE}/${id}/algorithms`;
};

export const createAlgorithmsRoute = (id) => {
  return `${BASEROUTE}/algorithms`;
};

export const deleteAlgorithmRoute = (id) => {
  return `${BASEROUTE}/algorithms/delete/${id}`;
};

export const reClassifyAlgorithmRoute = () => {
  return `${BASEROUTE}/algorithms/reclassify`;
};

export const getImplementationsRoute = (id) => {
  return `${BASEROUTE}/implementations/${id}`;
};

export const createImplementationRoute = () => {
  return `${BASEROUTE}/implementations`;
};

export const deleteImplementationRoute = (implementationId) => {
  return `${BASEROUTE}/implementations/delete/${implementationId}`;
};

export const getProblemInstancesRoute = (algorithmId) => {
  return `${BASEROUTE}/problem-instances?algorithmId=${algorithmId}`;
};

export const createProblemInstancesRoute = () => {
  return `${BASEROUTE}/problem-instances`;
};

export const deleteProblemInstancesRoute = (problemInstanceId) => {
  return `${BASEROUTE}/problem-instances/${problemInstanceId}`;
};

export const getUserHistoryRoute = (userId) => {
  return `${BASEROUTE}/user-history/${userId}`;
}

export const getUsersRoute = () => {
  return `${BASEROUTE}/list-user`;
}

export const internalSignUpRoute = () => {
  return `${BASEROUTE}/create-user`;
}

export const deleteUserRoute = () => {
  return `${BASEROUTE}/delete-user`;
}

