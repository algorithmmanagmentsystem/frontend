import { useEffect, useState, useCallback } from 'react';
import { getClassifications, postClassification } from '../apis/classification';

export const useClassifications = (open = false, classificationId = null) => {
  const [classifications, setClassifications] = useState([]);
  const [fetched, setFetched] = useState(false);
  const [loading, setLoading] = useState(false);
  const [createSuccess, setCreateSuccess] = useState(false);
  const [createLoading, setCreateLoading] = useState(false);

  const createClassification = useCallback(
    async (classificationName) => {
      let mounted = true;
      setCreateLoading(true);
      const response = await postClassification(classificationName);
      if (mounted) {
        setClassifications([...classifications, response]);
        setCreateLoading(false)
        setCreateSuccess(true)
      }
      return () => (mounted = false);
    },
    [setClassifications, classifications, setCreateLoading, setCreateSuccess],
  );

  useEffect(() => {
    async function fetchApi() {
      let mounted = true;
      if (open && !loading && !fetched) {
        setLoading(true);
        const response = await getClassifications();
        if (mounted) {
          setFetched(true);
          setLoading(false);
          setClassifications(response);
        }
      }
      return () => (mounted = false);
    }
    fetchApi();
  }, [setClassifications, classificationId, setFetched, setLoading, open, fetched, loading]);

  return [classifications, loading, createClassification, createLoading, createSuccess];
};
