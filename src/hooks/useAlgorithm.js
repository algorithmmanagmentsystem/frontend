import { useEffect, useState } from "react";
import { getAlgorithms } from "../apis/algorithm";

export const useAlgorithms = (open = false, classificationId = null) => {
  const [algorithms, setAlgorithms] = useState([]);
  const [fetched, setFetched] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function fetchApi() {
      if (open && !loading && !fetched) {
        setLoading(true);
        const response = await getAlgorithms();
        setFetched(true);
        setLoading(false);
        setAlgorithms(response);
      }
    }
    fetchApi();
  }, [setAlgorithms, classificationId, setFetched, setLoading, open, fetched, loading]);

  return [algorithms, loading];
};
