import { useState, useEffect, useCallback } from 'react';
import Classification from '../Classification';
import Tree, { TreeHeader } from '../../components/Tree';
import Button from '../../components/Button';
import AddClassificationModal from '../../components/AddClassificationModal';
import {
  deleteClassification,
  getClassifications,
  mergeClassifications,
  postClassification,
} from '../../apis/classification';
import Input from '../../components/Input';
import { reClassifyAlgorithm } from '../../apis/algorithm';

function Home({ user }) {
  // const [classifications, classificationsLoading, createClassification] = useClassifications(true);
  const [addClassificationModalOpen, setAddClassificationModalOpen] = useState(false);

  const [classifications, setClassifications] = useState([]);
  const [classificationsLoading, setClassificationsLoading] = useState(false);
  const [classificationsFetched, setClassificationsFetched] = useState(false);
  const [createLoading, setCreateLoading] = useState(false);
  const [selectedClassifications, setSelectedClassifications] = useState([]);
  const [selectedClassificationsParentId, setSelectedClassificationsParentId] = useState(undefined);
  const [isMerging, setIsMerging] = useState(false);
  const [newMergeClassificationName, setNewMergeClassificationName] = useState('');

  const [reClassifyingAlgorithmId, setReClassifyingAlgorithmId] = useState(null);

  const handleUpdateClassification = (parentClassificationId, classificationId, type) => {
    if (type === 'add') {
      if (isMerging) {
        if (selectedClassifications.length) {
          if (selectedClassificationsParentId === parentClassificationId) {
            setSelectedClassifications([...selectedClassifications, classificationId]);
          } else {
            setSelectedClassifications([classificationId]);
          }
        } else {
          setSelectedClassifications([classificationId]);
        }
      } else {
        setSelectedClassifications([classificationId]);
      }
      setSelectedClassificationsParentId(parentClassificationId);
    } else {
      const updatedSelectedClassifications = selectedClassifications.filter((c) => c !== classificationId);
      if (!updatedSelectedClassifications.length) {
        setSelectedClassificationsParentId(undefined);
      }
      setSelectedClassifications(updatedSelectedClassifications);
    }
  };

  useEffect(() => {
    async function fetchApi() {
      let mounted = true;
      if (!classificationsLoading && !classificationsFetched) {
        setClassificationsLoading(true);
        const response = await getClassifications();
        if (mounted) {
          setClassificationsFetched(true);
          setClassificationsLoading(false);
          setClassifications(response);
        }
      }
      return () => (mounted = false);
    }
    fetchApi();
  }, [classificationsLoading, classificationsFetched]);

  const createClassification = useCallback(
    async (classificationName) => {
      let mounted = true;
      if (!classificationName) {
        alert('Enter classification name');
        return () => (mounted = false);
      }
      setCreateLoading(true);
      const response = await postClassification(classificationName);
      if (mounted) {
        if (response) {
          setClassifications([...classifications, response]);
        }
        setCreateLoading(false);
        setAddClassificationModalOpen(false);
      }
      return () => (mounted = false);
    },
    [setClassifications, classifications, setCreateLoading, setAddClassificationModalOpen],
  );

  const handleAddClassification = () => {
    setAddClassificationModalOpen(true);
  };

  const handleCloseClassificationModal = () => {
    setAddClassificationModalOpen(false);
  };

  const handleBeginMerge = () => {
    setIsMerging(true);
  };

  const handleConfirmMerge = () => {
    async function fetchApi() {
      let mounted = true;
      console.log('loading');
      const response = await mergeClassifications(...selectedClassifications, newMergeClassificationName);
      setClassifications([]);
      setClassificationsLoading(true);
      const classificationsResponse = await getClassifications();
      console.log(response);
      if (mounted) {
        setClassificationsFetched(true);
        setClassificationsLoading(false);
        setClassifications(classificationsResponse);

        setSelectedClassifications([]);
        setSelectedClassificationsParentId(undefined);
        setIsMerging(false);
        setNewMergeClassificationName('');
      }
      return () => (mounted = false);
    }
    fetchApi();
  };

  const handleCancelMerge = () => {
    setIsMerging(false);
    setSelectedClassificationsParentId(undefined);
    setSelectedClassifications([]);
    setNewMergeClassificationName('');
  };

  const handleNewMergeClassificationNameChange = (e) => {
    setNewMergeClassificationName(e.target.value);
  };

  const handleBeginReClassifyAlgorithm = useCallback(
    (algorithmId) => {
      console.log('reclassifying...', algorithmId);
      setReClassifyingAlgorithmId(algorithmId);
    },
    [setReClassifyingAlgorithmId],
  );

  const handleCancelReClassifyAlgorithm = useCallback(() => {
    setReClassifyingAlgorithmId(null);
    setSelectedClassificationsParentId(undefined);
    setSelectedClassifications([]);
  }, []);

  const handleConfirmReClassifyAlgorithm = useCallback(() => {
    console.log(reClassifyingAlgorithmId, selectedClassifications);

    async function fetchApi() {
      let mounted = true;

      const response = await reClassifyAlgorithm(reClassifyingAlgorithmId, selectedClassifications[0]);

      setClassifications([]);
      setClassificationsLoading(true);
      const classificationsResponse = await getClassifications();

      console.log(response);
      if (mounted) {
        setClassificationsFetched(true);
        setClassificationsLoading(false);
        setClassifications(classificationsResponse);

        setReClassifyingAlgorithmId(null);
        setSelectedClassificationsParentId(undefined);
        setSelectedClassifications([]);
      }
      return () => (mounted = false);
    }

    fetchApi();
  }, [selectedClassifications, reClassifyingAlgorithmId]);

  const handleDeleteClassification = useCallback(
    async (classificationId) => {
      console.log(classificationId, 'delete');
      let mounted = true;
      // eslint-disable-next-line no-restricted-globals
      if (confirm('Are you sure you want to delete the classification?')) {
        const response = await deleteClassification(classificationId);
        if (mounted) {
          if (response) {
            setClassifications(classifications.filter((p) => p.id !== classificationId));
          }
        }
      }
      return () => (mounted = false);
    },
    [classifications],
  );

  return (
    <div className="home">
      <div className="container">
        {user && (
          <div className="row">
            {reClassifyingAlgorithmId ? (
              <>
                <div className="col py-2">
                  <Button
                    onClick={handleConfirmReClassifyAlgorithm}
                    type="primary"
                    disabled={selectedClassifications.length !== 1}
                  >
                    Confirm Re-Classify
                  </Button>
                  <Button onClick={handleCancelReClassifyAlgorithm} className="ms-2">
                    Cancel
                  </Button>
                </div>
                <div className="col-12 text-muted">
                  Select a classification to re-classify the algorithm to and click "Confirm Re-classify"
                </div>
              </>
            ) : (
              <div className="col py-2">
                {isMerging ? (
                  <div className="row">
                    <div className="col-4">
                      <Input
                        label="New Classification Name"
                        name="classificationName"
                        onChange={handleNewMergeClassificationNameChange}
                        value={newMergeClassificationName}
                      />
                    </div>
                    <div className="col d-flex align-items-end">
                      <div className="mb-2">
                        <Button
                          onClick={handleConfirmMerge}
                          type="primary"
                          disabled={selectedClassifications.length !== 2 || newMergeClassificationName.length === 0}
                        >
                          &#10003; Confirm Merge
                        </Button>
                        <Button onClick={handleCancelMerge} className="ms-3">
                          Cancel Merge
                        </Button>
                      </div>
                    </div>
                    <div className="col-12 text-muted">
                      Select two siblings classification, enter new name and press "Confirm Merge".
                    </div>
                  </div>
                ) : (
                  <Button onClick={handleBeginMerge}>Merge</Button>
                )}
              </div>
            )}
          </div>
        )}
        <div className="row">
          <div className="col">
            {classificationsLoading && <div>Loading</div>}
            {classifications.map((classification) => (
              <Classification
                key={classification.name}
                classification={classification}
                user={user}
                isMerging={isMerging}
                onUpdateClassification={handleUpdateClassification}
                selectedClassifications={selectedClassifications}
                reClassifyingAlgorithmId={reClassifyingAlgorithmId}
                onReClassifyAlgorithm={handleBeginReClassifyAlgorithm}
                onDeleteClassification={handleDeleteClassification}
              />
            ))}
            {user && (
              <Tree>
                <TreeHeader dashed onClick={handleAddClassification}>
                  Add Classification
                </TreeHeader>
              </Tree>
            )}
          </div>
        </div>
      </div>
      <AddClassificationModal
        show={addClassificationModalOpen}
        onClose={handleCloseClassificationModal}
        onSave={createClassification}
        loading={createLoading}
      />
    </div>
  );
}

export default Home;
