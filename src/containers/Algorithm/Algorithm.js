import { useCallback, useEffect, useState } from 'react';
import { deleteImplementation, getImplementations, postImplementation } from '../../apis/implementation';
import AddImplementationModal from '../../components/AddImplementationModal';
import Badge from '../../components/Badge';
import Tree, { TreeBody, TreeHeader } from '../../components/Tree';
import Implementation from '../Implementation';
// import { useAlgorithms } from '../../hooks/useAlgorithm';
import Button from '../../components/Button';

import './algorithm.scss';
import AlgorithmModal from '../../components/AlgorithmModal/AlgorithmModal';
import { deleteProblemInstance, getProblemInstances, postProblemInstance } from '../../apis/problemInstance';

function Algorithm({ algorithm, level = 0, user = null, reClassifyingAlgorithmId, onReClassify, onDeleteAlgorithm }) {
  const [isOpen, setIsOpen] = useState(false);
  const [algorithmModalOpen, setAlgorithmModalOpen] = useState(false);

  const [addImplementationModalOpen, setAddImplementationModalOpen] = useState(false);
  const [implementations, setImplementations] = useState([]);
  const [implementationsLoading, setImplementationsLoading] = useState(false);
  const [implementationsFetched, setImplementationsFetched] = useState(false);
  const [createImplementationLoading, setCreateImplementationLoading] = useState(false);

  const [problemInstancesLoading, setProblemInstancesLoading] = useState(false);
  const [problemInstances, setProblemInstances] = useState([]);

  const handleAddImplementation = () => {
    setAddImplementationModalOpen(true);
  };

  const handleToggleBody = useCallback(() => {
    setIsOpen(!isOpen);
  }, [setIsOpen, isOpen]);

  const handleCloseImplementationModal = useCallback(() => {
    setAddImplementationModalOpen(false);
  }, [setAddImplementationModalOpen]);

  const handleOpenAlgorithmModal = useCallback(() => {
    setAlgorithmModalOpen(true);
  }, [setAlgorithmModalOpen]);

  const handleCloseAlgorithmModal = useCallback(() => {
    setAlgorithmModalOpen(false);
  }, [setAlgorithmModalOpen]);

  const handleDeleteAlgorithm = useCallback(() => {
    onDeleteAlgorithm(algorithm.id);
  }, [onDeleteAlgorithm, algorithm]);

  const createImplementation = useCallback(
    async (implementationName, code, isUpload, implementationFileName, implementationMimeType) => {
      let mounted = true;
      if (!implementationName) {
        alert('Enter implementation name');
        return () => (mounted = false);
      }
      setCreateImplementationLoading(true);
      const response = await postImplementation(
        algorithm.id,
        implementationName,
        code,
        isUpload,
        implementationFileName,
        implementationMimeType,
      );
      if (mounted) {
        if (response) {
          setImplementations([...implementations, response]);
        }
        setCreateImplementationLoading(false);
        setAddImplementationModalOpen(false);
      }
      return () => (mounted = false);
    },
    [algorithm, setImplementations, implementations, setCreateImplementationLoading, setAddImplementationModalOpen],
  );

  useEffect(() => {
    async function fetchApi() {
      if (isOpen && !implementationsLoading && !implementationsFetched) {
        setImplementationsLoading(true);
        const response = await getImplementations(algorithm.id);
        setImplementationsFetched(true);
        setImplementationsLoading(false);
        setImplementations(response);
      }
    }
    fetchApi();
  }, [
    setImplementationsLoading,
    algorithm,
    setImplementationsFetched,
    implementationsFetched,
    implementationsLoading,
    isOpen,
    setImplementations,
  ]);

  useEffect(() => {
    async function fetchApi() {
      const response = await getProblemInstances(algorithm.id);
      setProblemInstances(response);
      setProblemInstancesLoading(false);
    }
    setProblemInstancesLoading(true);
    fetchApi();
  }, [algorithm, setProblemInstances, setProblemInstancesLoading]);

  const handleCreateProblemInstance = useCallback(
    async (name, input) => {
      let mounted = true;
      if (!name) {
        alert('Enter problem instance name');
        return () => (mounted = false);
      }
      if (!input) {
        alert('Enter problem instance');
        return () => (mounted = false);
      }
      setProblemInstancesLoading(true);

      const response = await postProblemInstance(algorithm.id, name, input);
      if (mounted) {
        if (response) {
          setProblemInstances([...problemInstances, response]);
        }
        setProblemInstancesLoading(false);
      }
      return () => (mounted = false);
    },
    [algorithm, problemInstances, setProblemInstancesLoading],
  );

  const handleDeleteProblemInstance = useCallback(
    async (problemInstanceId) => {
      let mounted = true;
      console.log(problemInstanceId);
      // eslint-disable-next-line no-restricted-globals
      if (confirm('Are you sure you want to delete the problem instance?')) {
        const response = await deleteProblemInstance(problemInstanceId);
        if (mounted) {
          if (response) {
            setProblemInstances(problemInstances.filter((p) => p.id !== problemInstanceId));
          }
        }
      }

      return () => (mounted = false);
    },
    [problemInstances],
  );

  const handleDeleteImplementation = useCallback(async (implementationId) => {
    let mounted = true;
    // eslint-disable-next-line no-restricted-globals
    if (confirm('Are you sure you want to delete the implementation?')) {
      const response = await deleteImplementation(implementationId);
      if (mounted) {
        if (response) {
          setImplementations(implementations.filter((i) => i.id !== implementationId));
        }
      }
    }
    return () => (mounted = false);
  }, [implementations]);

  const handleReClassify = useCallback(() => {
    onReClassify(algorithm.id);
  }, [algorithm, onReClassify]);

  return (
    <>
      <AddImplementationModal
        show={addImplementationModalOpen}
        onClose={handleCloseImplementationModal}
        onSave={createImplementation}
        loading={createImplementationLoading}
      />
      <AlgorithmModal
        show={algorithmModalOpen}
        onSaveProblemInstance={handleCreateProblemInstance}
        problemInstances={problemInstances}
        loading={problemInstancesLoading}
        onClose={handleCloseAlgorithmModal}
        algorithmName={algorithm.name}
        onDeleteProblemInstance={handleDeleteProblemInstance}
        user={user}
      />
      <Tree
        level={level}
        className={`algorithm ${reClassifyingAlgorithmId === algorithm.id ? 'algorithm--reclassifying' : ''}`}
      >
        <TreeHeader>
          <div className="algorithm__header">
            <div
              className={`algorithm__toggle ${isOpen ? 'algorithm__toggle--open' : ''}`}
              onClick={handleToggleBody}
            ></div>
            <div className="flex-grow-1">
              <Button type="link" className="algorithm__name d-flex" onClick={handleOpenAlgorithmModal}>
                {algorithm.name}
              </Button>
            </div>
            <Badge type="algorithm" />
            {user && (
              <>
                {!reClassifyingAlgorithmId ? (
                  <Button type="outline-secondary" className="ms-2" size="small" noBorder onClick={handleReClassify}>
                    Re-Classify
                  </Button>
                ) : (
                  <>{reClassifyingAlgorithmId === algorithm.id ? 'Re-classifying...' : null}</>
                )}
              </>
            )}
            {user && user.isAdmin && (
              <Button type="outline-danger" className="ms-2" size="small" noBorder onClick={handleDeleteAlgorithm}>
                Delete
              </Button>
            )}
          </div>
        </TreeHeader>
        <TreeBody isOpen={isOpen} isLoading={implementationsLoading}>
          {implementations.map((implementation) => (
            <Implementation
              key={implementation.id}
              implementation={implementation}
              problemInstances={problemInstances}
              onDelete={handleDeleteImplementation}
              user={user}
            />
          ))}
          {user && (
            <>
              <Tree level={level + 1}>
                <TreeHeader dashed onClick={handleAddImplementation}>
                  <div className="text-truncate">Add Implementation (under {algorithm.name})</div>
                </TreeHeader>
              </Tree>
            </>
          )}
        </TreeBody>
      </Tree>
    </>
  );
}

export default Algorithm;
