import { useCallback, useState, useEffect } from 'react';
import Tree, { TreeBody, TreeHeader } from '../../components/Tree';
// import { useAlgorithms } from '../../hooks/useAlgorithm';
import Algorithm from '../Algorithm';
import AddClassificationModal from '../../components/AddClassificationModal';
import AddAlgorithmModal from '../../components/AddAlgorithmModal';
import { deleteClassification, getClassifications, postClassification } from '../../apis/classification';
import { deleteAlgorithm, getAlgorithms, postAlgorithm } from '../../apis/algorithm';
import Badge from '../../components/Badge';
import Button from '../../components/Button';

import './classification.scss';

function Classification({
  classification,
  level = 0,
  user,
  onUpdateClassification,
  selectedClassifications,
  isMerging,
  reClassifyingAlgorithmId,
  onReClassifyAlgorithm,
  onDeleteClassification
}) {
  const [isOpen, setIsOpen] = useState(false);
  const [addClassificationModalOpen, setAddClassificationModalOpen] = useState(false);

  const [childClassifications, setChildClassifications] = useState([]);
  const [childClassificationsLoading, setChildClassificationsLoading] = useState(false);
  const [childClassificationsFetched, setChildClassificationsFetched] = useState(false);
  const [createChildClassificationLoading, setCreateChildClassificationLoading] = useState(false);

  const [addAlgorithmModalOpen, setAddAlgorithmModalOpen] = useState(false);
  const [algorithms, setAlgorithms] = useState([]);
  const [algorithmsLoading, setAlgorithmsLoading] = useState(false);
  const [algorithmsFetched, setAlgorithmsFetched] = useState(false);
  const [createAlgorithmLoading, setCreateAlgorithmLoading] = useState(false);

  const handleAddClassification = () => {
    setAddClassificationModalOpen(true);
  };

  const handleAddAlgorithm = () => {
    setAddAlgorithmModalOpen(true);
  };

  const handleToggleBody = useCallback(() => {
    setIsOpen(!isOpen);
  }, [setIsOpen, isOpen]);

  const handleCloseClassificationModal = useCallback(() => {
    setAddClassificationModalOpen(false);
  }, [setAddClassificationModalOpen]);

  const handleCloseAlgorithmModal = useCallback(() => {
    setAddAlgorithmModalOpen(false);
  }, [setAddAlgorithmModalOpen]);

  useEffect(() => {
    async function fetchApi() {
      let mounted = true;
      if (isOpen && !childClassificationsLoading && !childClassificationsFetched) {
        setChildClassificationsLoading(true);
        const response = await getClassifications(classification.id);
        if (mounted) {
          setChildClassificationsFetched(true);
          setChildClassificationsLoading(false);
          setChildClassifications(response);
        }
      }
      return () => (mounted = false);
    }
    fetchApi();
  }, [isOpen, childClassificationsLoading, childClassificationsFetched, classification]);

  const createClassification = useCallback(
    async (classificationName) => {
      let mounted = true;
      if (!classificationName) {
        alert('Enter classification name');
        return () => (mounted = false);
      }
      setCreateChildClassificationLoading(true);
      const response = await postClassification(classificationName, classification.id);
      if (mounted) {
        if (response) {
          setChildClassifications([...childClassifications, response]);
        }

        // setChildClassifications([...childClassifications, response]);
        setCreateChildClassificationLoading(false);
        setAddClassificationModalOpen(false);
      }
      return () => (mounted = false);
    },
    [
      classification,
      setChildClassifications,
      childClassifications,
      setCreateChildClassificationLoading,
      setAddClassificationModalOpen,
    ],
  );

  const createAlgorithm = useCallback(
    async (algorithmName) => {
      let mounted = true;
      if (!algorithmName) {
        alert('Enter algorithm name');
        return () => (mounted = false);
      }
      setCreateAlgorithmLoading(true);
      const response = await postAlgorithm(classification.id, algorithmName);
      if (mounted) {
        setAlgorithms([...algorithms, response]);
        setCreateAlgorithmLoading(false);
        setAddAlgorithmModalOpen(false);
      }
      return () => (mounted = false);
    },
    [algorithms, classification, setAlgorithms, setCreateAlgorithmLoading, setAddAlgorithmModalOpen],
  );

  useEffect(() => {
    async function fetchApi() {
      if (isOpen && !algorithmsLoading && !algorithmsFetched) {
        setAlgorithmsLoading(true);
        const response = await getAlgorithms(classification.id);
        setAlgorithmsFetched(true);
        setAlgorithmsLoading(false);
        setAlgorithms(response);
      }
    }
    fetchApi();
  }, [
    setAlgorithmsLoading,
    classification,
    setAlgorithmsFetched,
    algorithmsFetched,
    algorithmsLoading,
    isOpen,
    setAlgorithms,
  ]);

  const handleSelectClassification = (e) => {
    onUpdateClassification(classification.parentId, classification.id, e.target.checked ? 'add' : 'remove');
  };

  const handleDeleteChildClassification = useCallback(async (classificationId) => {
    console.log(classificationId, 'delete');
    let mounted = true;
    // eslint-disable-next-line no-restricted-globals
    if (confirm('Are you sure you want to delete the classification?')) {
      const response = await deleteClassification(classificationId);
      if (mounted) {
        if (response) {
          setChildClassifications(childClassifications.filter((p) => p.id !== classificationId));
        }
      }
    }

    return () => (mounted = false);
  }, [childClassifications]);

  const handleDeleteAlgorithm = useCallback(async (algorithmId) => {
    console.log(algorithmId, 'delete');
    let mounted = true;
    // eslint-disable-next-line no-restricted-globals
    if (confirm('Are you sure you want to delete the algorithm?')) {
      const response = await deleteAlgorithm(algorithmId);
      if (mounted) {
        if (response) {
          setAlgorithms(algorithms.filter((p) => p.id !== algorithmId));
        }
      }
    }
    return () => (mounted = false);
  }, [algorithms]);

  const handleDeleteClassification = useCallback(async () => {
    onDeleteClassification(classification.id)
  }, [onDeleteClassification, classification]);


  // const handleDeleteClassification = useCallback(async () => {
  //   console.log(classification.id, 'delete');
  //   const classificationId = classification.id;
  //   let mounted = true;
  //   // eslint-disable-next-line no-restricted-globals
  //   if (confirm('Are you sure you want to delete the problem instance?')) {
  //     const response = await deleteClassification(classificationId);
  //     if (mounted) {
  //       if (response) {
  //         setChildClassifications(childClassifications.filter((p) => p.id !== classificationId));
  //       }
  //     }
  //   }

  //   return () => (mounted = false);
  // }, [childClassifications, classification]);


  return (
    <>
      <AddClassificationModal
        show={addClassificationModalOpen}
        onClose={handleCloseClassificationModal}
        onSave={createClassification}
        loading={createChildClassificationLoading}
      />
      <AddAlgorithmModal
        show={addAlgorithmModalOpen}
        onClose={handleCloseAlgorithmModal}
        onSave={createAlgorithm}
        loading={createAlgorithmLoading}
      />
      <Tree level={level} className="classification">
        <TreeHeader>
          <div className="classification__header">
            <div
              className={`classification__toggle ${isOpen ? 'classification__toggle--open' : ''}`}
              onClick={handleToggleBody}
            ></div>
            <div className="flex-grow-1">
              {(isMerging || !!reClassifyingAlgorithmId) && (
                <input
                  type="checkbox"
                  onChange={handleSelectClassification}
                  checked={selectedClassifications.indexOf(classification.id) !== -1}
                />
              )}{' '}
              {classification.name}
            </div>
            <Badge type="classification" />
            {user && user.isAdmin && (
              <Button type="outline-danger" className="ms-2" size="small" noBorder onClick={handleDeleteClassification}>
                Delete
              </Button>
            )}
          </div>
        </TreeHeader>
        <TreeBody isOpen={isOpen} isLoading={!childClassificationsFetched || !algorithmsFetched}>
          {childClassifications.map((childClassification) => (
            <Classification
              key={childClassification.name}
              classification={childClassification}
              onUpdateClassification={onUpdateClassification}
              selectedClassifications={selectedClassifications}
              onReClassifyAlgorithm={onReClassifyAlgorithm}
              isMerging={isMerging}
              level={level + 1}
              user={user}
              reClassifyingAlgorithmId={reClassifyingAlgorithmId}
              onDeleteClassification={handleDeleteChildClassification}
            />
          ))}
          {algorithms.map((algorithm) => (
            <Algorithm
              key={algorithm.name}
              algorithm={algorithm}
              level={level + 1}
              onReClassify={onReClassifyAlgorithm}
              reClassifyingAlgorithmId={reClassifyingAlgorithmId}
              onDeleteAlgorithm={handleDeleteAlgorithm}
              user={user}
            />
          ))}
          {user && (
            <>
              <Tree level={level + 1}>
                <TreeHeader dashed onClick={handleAddClassification}>
                  <div className="text-truncate">Add Classification (under {classification.name})</div>
                </TreeHeader>
              </Tree>
              <Tree level={level + 1}>
                <TreeHeader dashed onClick={handleAddAlgorithm}>
                  <div className="text-truncate">Add Algorithm (under {classification.name})</div>
                </TreeHeader>
              </Tree>
            </>
          )}
        </TreeBody>
      </Tree>
    </>
  );
}

export default Classification;
