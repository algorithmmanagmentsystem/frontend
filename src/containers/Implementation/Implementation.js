import { useCallback, useState } from 'react';
import Badge from '../../components/Badge';
import Tree, { TreeHeader } from '../../components/Tree';
import Button from '../../components/Button';
import ImplementationModal from '../../components/ImplementationModal';

import './implementation.scss';

function Implementation({ implementation, problemInstances, user, onDelete}) {
  // const [isOpen, setIsOpen] = useState(false);
  const [algorithmModalOpen, setAlgorithmModalOpen] = useState(false);
  // const []
  // const [algorithms, implementationssLoading] = useAlgorithms(isOpen);

  // const handleToggleBody = useCallback(() => {
  //   setIsOpen(!isOpen);
  // }, [setIsOpen, isOpen]);
  const handleCreateProblemInstance = () => {
    console.log('handleCreateProblemInstance');
  };

  const handleCloseImplementationModal = () => {
    setAlgorithmModalOpen(false);
  };

  const handleOpenImplementationModal = () => {
    setAlgorithmModalOpen(true);
  };

  const handleDeleteImplementation = () => {
    onDelete(implementation.id)
  }

  return (
    <>
      <ImplementationModal
        show={algorithmModalOpen}
        implementation={implementation}
        problemInstances={problemInstances}
        benchmarks={[]}
        onSaveProblemInstance={handleCreateProblemInstance}
        loading={false}
        onClose={handleCloseImplementationModal}
        // algorithmName={algorithm.name}
        // onDeleteBenchmark={handleDeleteBenchmark}
        user={user}
      />

      <Tree level={1} className="implementation">
        <TreeHeader>
          <div className="implementation__header">
            {/* <div
              className={`implementation__toggle ${isOpen ? 'implementation__toggle--open' : ''}`}
              onClick={handleToggleBody}
            ></div> */}
            <div className="flex-grow-1">
              {/* handleOpenImplementationModal */}
              <Button type="link" className="implementation__name d-flex" onClick={handleOpenImplementationModal}>
                {implementation.name}
              </Button>

              {/* {implementation.name}{' '} */}
              {/* <a href={implementation.s3Url} download>
                Download
              </a> */}
            </div>
            <Badge type="implementation" />
            {user && user.isAdmin && (
              <Button type="outline-danger" className="ms-2" size="small" noBorder onClick={handleDeleteImplementation}>
                Delete
              </Button>
            )}
          </div>
        </TreeHeader>
        {/* <TreeBody isOpen={isOpen} isLoading={implementationsLoading}>
          {algorithms.map((algorithm) => (
            <div key={algorithm.name}>{algorithm.name}</div>
          ))}
        </TreeBody> */}
      </Tree>
    </>
  );
}

export default Implementation;
