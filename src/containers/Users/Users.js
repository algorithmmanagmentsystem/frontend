import { useState, useEffect, useCallback } from 'react';
// import Classification from '../Classification';
// import Tree, { TreeHeader } from '../../components/Tree';
// import Button from '../../components/Button';
// import AddClassificationModal from '../../components/AddClassificationModal';
// import { getClassifications, postClassification } from '../../apis/classification';
import { Redirect } from 'react-router-dom';
import { deleteUser, getUserHistory, getUsers } from '../../apis/user';
import Button from '../../components/Button';

import './users.scss';

function Users({ user }) {
  const [users, setUsers] = useState([]);
  const [loadingUsers, setLoadingUsers] = useState(false);
  const [selectedUserId, setSelectedUserId] = useState(null);
  const [userHistory, setUserHistory] = useState([]);
  const [userHistoryLoading, setUserHistoryLoading] = useState(false);

  useEffect(() => {
    async function fetchApi() {
      let mounted = true;
      if (user && user.isAdmin) {
        console.log('fetching users');
        const users = await getUsers();
        if (mounted) {
          setUsers(users);
        }
        // console.log(users)
        // setUsers([
        //   {
        //     username: 'hilson',
        //     id: 'bd86c223-6d89-4eb1-9986-f4046ac81df3',
        //     name: 'Hilson Shrestha',
        //   },
        //   {
        //     username: 'hilson2',
        //     id: '1231231232',
        //     name: 'Hilson Shrestha2',
        //   },
        // ]);
      }
      return () => (mounted = false);
    }
    fetchApi();
  }, [user]);

  const handleSelectUser = (id) => {
    setSelectedUserId(id);

    async function fetchApi() {
      let mounted = true;

      if (id !== selectedUserId) {
        setUserHistoryLoading(true);
        const response = await getUserHistory(id);
        console.log(response);
        if (mounted) {
          setUserHistory(response);
          setUserHistoryLoading(false);
        }
      }
      return () => (mounted = false);
    }
    fetchApi();
  };

  if (!user || !user.isAdmin) {
    return <Redirect to="/" />;
  }

  const handleDelete = async(id) => {
      let mounted = true;
      // eslint-disable-next-line no-restricted-globals
      if (confirm('Are you sure you want to delete the user?')) {
        await deleteUser(id);
        if (mounted) {
          setUsers(users.filter((p) => p.id !== id));
        }
      }

      return () => (mounted = false);
  };

  return (
    <div className="users">
      <div className="container">
        <div className="row">
          <div className="col">
            {users.map((user) => (
              <div
                key={user.id}
                className={`user ${selectedUserId === user.id ? 'user--active' : ''}`}
                onClick={() => handleSelectUser(user.id)}
              >
                <div className="user__username">{user.username}</div>
                <div className="user__name">{user.name}</div>
                <div className="user__email">{user.email}</div>
                <div>
                  <Button type="outline-danger" onClick={() => {handleDelete(user.userId)}}>
                    Delete
                  </Button>
                </div>
              </div>
            ))}
          </div>
          <div className="col user-history">
            {userHistoryLoading ? (
              <div className="spinner-border text-secondary" role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
            ) : (
              <>
                {userHistory.map((history, idx) => (
                  <div key={idx} className="user-history__item">
                    <div className="history__timestamp">{history.timestamp}</div>
                    <div className="history__activity">{history.activity}</div>
                  </div>
                ))}
              </>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Users;
