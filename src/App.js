import { useCallback, useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { postLogin, postRegister, postSignOut } from './apis/account';
import './App.scss';
import Header from './components/Header';
import Home from './containers/Home';
import Login from './containers/Login';
import Register from './containers/Register';
import Users from './containers/Users';

function App() {
  const [userLoginLoading, setUserLoginLoading] = useState(false);
  const [userRegisterLoading, setUserRegisterLoading] = useState(false);

  const [user, setUser] = useState();

  useEffect(() => {
    // check of user data in local storage.
    try {
      setUser(JSON.parse(localStorage.getItem('user')));
    } catch (err) {
      setUser(undefined);
    }
  }, []);

  const handleUserLogin = useCallback(
    async (username, password) => {
      let mounted = true;
      setUserLoginLoading(true);
      try {
        const response = await postLogin(username, password);
        if (mounted) {
          setUser(response);
          setUserLoginLoading(false);
        }
      } catch (err) {
        if (mounted) {
          alert(err.message);
          setUserLoginLoading(false);
        }
      }
      return () => (mounted = false);
    },
    [setUserLoginLoading, setUser],
  );

  const handleUserRegister = useCallback(
    async (name, email, username, password) => {
      let mounted = true;
      setUserRegisterLoading(true);
      try {
        const response = await postRegister(name, email, username, password);
        if (mounted) {
          setUser(response);
          setUserRegisterLoading(false);
        }
      } catch (err) {
        if (mounted) {
          alert(err.message);
          setUserRegisterLoading(false);
        }
      }
      return () => (mounted = false);
    },
    [setUserRegisterLoading, setUser],
  );

  const handleUserSignOut = useCallback(async () => {
    await postSignOut();
    setUser(undefined);
  }, [setUser]);

  return (
    <div className="App">
      <Router>
        <Header
          user={user}
          onLogin={handleUserLogin}
          loginLoading={userLoginLoading}
          registerLoading={userRegisterLoading}
          onRegister={handleUserRegister}
          onSignOut={handleUserSignOut}
        />
        <Switch>
          <Route path="/register">
            <Register />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/users">
            <Users user={user} />
          </Route>
          <Route path="/">
            <Home user={user} />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
